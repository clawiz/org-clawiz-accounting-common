package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsModelOperationPrototype extends org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation {
    
    public String javaName;
    
    public String getJavaName() {
        return this.javaName;
    }
    
    public void setJavaName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.javaName = null;
        }
        this.javaName = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845845899";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.ModelOperation";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "4479303018845845899", "org.clawiz.accounting.common.language.structure.ModelOperation", ConceptPropertyType.PROPERTY, "2393639942922422792", "javaName"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeProperty("4479303018845845899", "javaName", getJavaName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.operation.ModelOperation.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.model.operation.ModelOperation structure = (org.clawiz.accounting.common.metadata.data.model.operation.ModelOperation) node;
        
        structure.setJavaName(getJavaName());
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.model.operation.ModelOperation structure = (org.clawiz.accounting.common.metadata.data.model.operation.ModelOperation) node;
        
        setJavaName(structure.getJavaName());
        
    }
}
