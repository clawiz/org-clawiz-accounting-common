package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractModelOperationPrototype extends AbstractMpsNode {
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsModelOperationSign sign;
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsModelOperationSign getSign() {
        return this.sign;
    }
    
    public void setSign(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.sign = null;
        }
        this.sign = org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsModelOperationSign.toMpsModelOperationSign(value);
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845845883";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.AbstractModelOperation";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "4479303018845845883", "org.clawiz.accounting.common.language.structure.AbstractModelOperation", ConceptPropertyType.PROPERTY, "4479303018845845894", "sign"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("4479303018845845883", "sign", org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsModelOperationSign.toConceptNodePropertyString(getSign()));
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation structure = (org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation) node;
        
        structure.setSign(( getSign() != null ? org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationSign.valueOf(getSign().toString()) : null ));
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation structure = (org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation) node;
        
        setSign(structure.getSign() != null ? structure.getSign().toString() : null);
        
    }
}
