package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAccountOperationsTypeMethodActionPrototype extends org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.method.action.MpsAbstractTypeMethodAction {
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.MpsAccountOperation> operations = new ArrayList<>();
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.MpsAccountOperation> getOperations() {
        return this.operations;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845859171";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.AccountOperationsTypeMethodAction";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "4479303018845859171", "org.clawiz.accounting.common.language.structure.AccountOperationsTypeMethodAction", ConceptPropertyType.CHILD, "2393639942922434548", "operations"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        for (AbstractMpsNode value : getOperations() ) {
            addConceptNodeChild("4479303018845859171", "operations", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationsTypeMethodAction.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationsTypeMethodAction structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationsTypeMethodAction) node;
        
        structure.getOperations().clear();
        for (org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.MpsAccountOperation mpsNode : getOperations() ) {
            if ( mpsNode != null ) {
                structure.getOperations().add((org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation) mpsNode.toMetadataNode(structure, "operations"));
            } else {
                structure.getOperations().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationsTypeMethodAction structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationsTypeMethodAction) node;
        
        getOperations().clear();
        for (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation metadataNode : structure.getOperations() ) {
            if ( metadataNode != null ) {
                getOperations().add(loadChildMetadataNode(metadataNode));
            } else {
                getOperations().add(null);
            }
        }
        
    }
}
