package org.clawiz.accounting.common.metadata.data.model.operation;

import org.clawiz.core.common.CoreException;

public enum ModelOperationSign {

    PLUS, MINUS;

    
    public static ModelOperationSign toModelOperationSign(String value) {
        if ( value == null ) {
            return null;
        }
        
        try {
            return ModelOperationSign.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong ModelOperationSign value '?'", value);
        }
        
    }
    
    public static String toDescription(ModelOperationSign value) {
        if ( value == null ) {
            return null;
        }
        
        switch (value) {
            case PLUS : return "+";
            case MINUS: return "-";
        }
        
        return value.toString();
    }
}
