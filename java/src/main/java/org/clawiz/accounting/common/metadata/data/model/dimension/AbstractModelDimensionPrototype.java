package org.clawiz.accounting.common.metadata.data.model.dimension;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractModelDimensionPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractModelDimension withName(String value) {
        setName(value);
        return (AbstractModelDimension) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
