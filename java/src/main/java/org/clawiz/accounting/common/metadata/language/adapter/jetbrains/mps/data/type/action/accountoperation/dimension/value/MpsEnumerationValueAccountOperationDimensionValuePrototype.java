package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsEnumerationValueAccountOperationDimensionValuePrototype extends org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsAbstractAccountOperationDimensionValue {
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue value;
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue getValue() {
        return this.value;
    }
    
    public void setValue(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue value) {
        this.value = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2393639942922588220";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.EnumerationValueAccountOperationDimensionValue";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922588220", "org.clawiz.accounting.common.language.structure.EnumerationValueAccountOperationDimensionValue", ConceptPropertyType.REFERENCE, "2393639942922588221", "value"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("2393639942922588220", "value", getValue());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.EnumerationValueAccountOperationDimensionValue.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.EnumerationValueAccountOperationDimensionValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.EnumerationValueAccountOperationDimensionValue) node;
        
        if ( getValue() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "value", false, getValue());
        } else {
            structure.setValue(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.EnumerationValueAccountOperationDimensionValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.EnumerationValueAccountOperationDimensionValue) node;
        
        if ( structure.getValue() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "value", false, structure.getValue());
        } else {
            setValue(null);
        }
        
    }
}
