package org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractAccountOperationAmountValuePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractAccountOperationAmountValue withName(String value) {
        setName(value);
        return (AbstractAccountOperationAmountValue) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
