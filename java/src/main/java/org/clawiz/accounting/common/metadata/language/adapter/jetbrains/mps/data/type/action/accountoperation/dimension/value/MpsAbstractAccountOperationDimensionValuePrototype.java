package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractAccountOperationDimensionValuePrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2393639942922475013";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.AbstractAccountOperationDimensionValue";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue) node;
        
    }
}
