package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeFieldAccountOperationDimensionValuePrototype extends org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsAbstractAccountOperationDimensionValue {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField field;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField getField() {
        return this.field;
    }
    
    public void setField(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField value) {
        this.field = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2393639942922487034";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.TypeFieldAccountOperationDimensionValue";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922487034", "org.clawiz.accounting.common.language.structure.TypeFieldAccountOperationDimensionValue", ConceptPropertyType.REFERENCE, "2393639942922487035", "field"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("2393639942922487034", "field", getField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.TypeFieldAccountOperationDimensionValue.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.TypeFieldAccountOperationDimensionValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.TypeFieldAccountOperationDimensionValue) node;
        
        if ( getField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "field", false, getField());
        } else {
            structure.setField(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.TypeFieldAccountOperationDimensionValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.TypeFieldAccountOperationDimensionValue) node;
        
        if ( structure.getField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "field", false, structure.getField());
        } else {
            setField(null);
        }
        
    }
}
