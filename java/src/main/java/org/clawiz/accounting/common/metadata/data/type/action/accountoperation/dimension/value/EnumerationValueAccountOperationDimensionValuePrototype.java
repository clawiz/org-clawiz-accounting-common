package org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class EnumerationValueAccountOperationDimensionValuePrototype extends org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue {
    
    @ExchangeReference
    private org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue value;
    
    public EnumerationValueAccountOperationDimensionValue withName(String value) {
        setName(value);
        return (EnumerationValueAccountOperationDimensionValue) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue getValue() {
        return this.value;
    }
    
    public void setValue(org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue value) {
        this.value = value;
    }
    
    public EnumerationValueAccountOperationDimensionValue withValue(org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue value) {
        setValue(value);
        return (EnumerationValueAccountOperationDimensionValue) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValue() != null ) { 
            getValue().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValue());
        
    }
}
