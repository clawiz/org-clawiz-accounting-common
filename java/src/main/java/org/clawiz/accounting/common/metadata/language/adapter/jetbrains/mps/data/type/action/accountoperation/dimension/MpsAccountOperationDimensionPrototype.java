package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAccountOperationDimensionPrototype extends AbstractMpsNode {
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsAbstractAccountOperationDimensionValue value;
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension dimension;
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsAbstractAccountOperationDimensionValue getValue() {
        return this.value;
    }
    
    public void setValue(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsAbstractAccountOperationDimensionValue value) {
        this.value = value;
    }
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension getDimension() {
        return this.dimension;
    }
    
    public void setDimension(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension value) {
        this.dimension = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2393639942922416139";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.AccountOperationDimension";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922416139", "org.clawiz.accounting.common.language.structure.AccountOperationDimension", ConceptPropertyType.CHILD, "2393639942922475052", "value"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922416139", "org.clawiz.accounting.common.language.structure.AccountOperationDimension", ConceptPropertyType.REFERENCE, "2393639942922473273", "dimension"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeChild("2393639942922416139", "value", getValue());
        addConceptNodeRef("2393639942922416139", "dimension", getDimension());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension) node;
        
        if ( getValue() != null ) {
            structure.setValue((org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue) getValue().toMetadataNode(structure, "value"));
        } else {
            structure.setValue(null);
        }
        
        if ( getDimension() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "dimension", false, getDimension());
        } else {
            structure.setDimension(null);
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension) node;
        
        if ( structure.getValue() != null ) {
            setValue(loadChildMetadataNode(structure.getValue()));
        } else {
            setValue(null);
        }
        
        if ( structure.getDimension() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "dimension", false, structure.getDimension());
        } else {
            setDimension(null);
        }
        
    }
}
