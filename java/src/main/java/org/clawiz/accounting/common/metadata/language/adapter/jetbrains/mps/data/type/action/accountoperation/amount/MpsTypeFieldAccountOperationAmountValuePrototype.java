package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsTypeFieldAccountOperationAmountValuePrototype extends org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount.MpsAbstractAccountOperationAmountValue {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField field;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField getField() {
        return this.field;
    }
    
    public void setField(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.field.MpsTypeField value) {
        this.field = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2393639942922551227";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.TypeFieldAccountOperationAmountValue";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922551227", "org.clawiz.accounting.common.language.structure.TypeFieldAccountOperationAmountValue", ConceptPropertyType.REFERENCE, "2393639942922551228", "field"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("2393639942922551227", "field", getField());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.TypeFieldAccountOperationAmountValue.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.TypeFieldAccountOperationAmountValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.TypeFieldAccountOperationAmountValue) node;
        
        if ( getField() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "field", false, getField());
        } else {
            structure.setField(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.TypeFieldAccountOperationAmountValue structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.TypeFieldAccountOperationAmountValue) node;
        
        if ( structure.getField() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "field", false, structure.getField());
        } else {
            setField(null);
        }
        
    }
}
