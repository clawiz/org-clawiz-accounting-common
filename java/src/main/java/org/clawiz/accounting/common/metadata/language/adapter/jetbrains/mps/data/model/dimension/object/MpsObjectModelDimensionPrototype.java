package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.object;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsObjectModelDimensionPrototype extends org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension {
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType referencedType;
    
    public org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType getReferencedType() {
        return this.referencedType;
    }
    
    public void setReferencedType(org.clawiz.core.common.metadata.language.adapter.jetbrains.mps.data.type.MpsType value) {
        this.referencedType = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845649840";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.ObjectModelDimension";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "4479303018845649840", "org.clawiz.accounting.common.language.structure.ObjectModelDimension", ConceptPropertyType.REFERENCE, "4479303018845649841", "referencedType"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        addConceptNodeRef("4479303018845649840", "referencedType", getReferencedType());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.dimension.object.ObjectModelDimension.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.model.dimension.object.ObjectModelDimension structure = (org.clawiz.accounting.common.metadata.data.model.dimension.object.ObjectModelDimension) node;
        
        if ( getReferencedType() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "referencedType", false, getReferencedType());
        } else {
            structure.setReferencedType(null);
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
        addForeignKey(getReferencedType());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.model.dimension.object.ObjectModelDimension structure = (org.clawiz.accounting.common.metadata.data.model.dimension.object.ObjectModelDimension) node;
        
        if ( structure.getReferencedType() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "referencedType", false, structure.getReferencedType());
        } else {
            setReferencedType(null);
        }
        
    }
}
