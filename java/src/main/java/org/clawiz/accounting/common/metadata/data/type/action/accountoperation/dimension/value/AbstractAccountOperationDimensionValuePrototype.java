package org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value;

import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractAccountOperationDimensionValuePrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    public AbstractAccountOperationDimensionValue withName(String value) {
        setName(value);
        return (AbstractAccountOperationDimensionValue) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
