package org.clawiz.accounting.common.metadata.data.model.dimension.enumeration;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class EnumerationModelDimensionPrototype extends org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension {
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValueList values = new org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValueList();
    
    public EnumerationModelDimension withName(String value) {
        setName(value);
        return (EnumerationModelDimension) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValueList getValues() {
        return this.values;
    }
    
    public EnumerationModelDimension withValue(org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue value) {
        getValues().add(value);
        return (EnumerationModelDimension) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue> T createValue(Class<T> nodeClass) {
        org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue value = createChildNode(nodeClass, "values");
        getValues().add(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue createValue() {
        return createValue(org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getValues()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getValues()) {
            references.add(node);
        }
        
    }
}
