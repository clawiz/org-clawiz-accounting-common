package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation;

import org.clawiz.core.common.CoreException;

public enum MpsModelOperationSign {

    PLUS, MINUS;

    
    public static MpsModelOperationSign getDefaultValue() {
        return PLUS;
    }
    
    public static MpsModelOperationSign toMpsModelOperationSign(String string) {
        if ( string == null ) {
            return null;
        }
        
        try {
            return MpsModelOperationSign.valueOf(string.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new CoreException("Wrong MpsModelOperationSign value '?", string);
        }
        
    }
    
    public static String toConceptNodePropertyString(MpsModelOperationSign value) {
        if ( value == null ) { 
            return null;
        }
         
        switch (value) {
            case PLUS : return "PLUS";
            case MINUS : return "MINUS";
        }
         
        return null;
    }
}
