package org.clawiz.accounting.common.metadata.data.model;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ModelPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private String description;
    
    @ExchangeAttribute
    private String javaName;
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.model.dimension.ModelDimensionList dimensions = new org.clawiz.accounting.common.metadata.data.model.dimension.ModelDimensionList();
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationList operations = new org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationList();
    
    public Model withName(String value) {
        setName(value);
        return (Model) this;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        this.description = value;
    }
    
    public Model withDescription(String value) {
        setDescription(value);
        return (Model) this;
    }
    
    public String getJavaName() {
        return this.javaName;
    }
    
    public void setJavaName(String value) {
        this.javaName = value;
    }
    
    public Model withJavaName(String value) {
        setJavaName(value);
        return (Model) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.dimension.ModelDimensionList getDimensions() {
        return this.dimensions;
    }
    
    public Model withDimension(org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension value) {
        getDimensions().add(value);
        return (Model) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension> T createDimension(Class<T> nodeClass) {
        org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension value = createChildNode(nodeClass, "dimensions");
        getDimensions().add(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension createDimension() {
        return createDimension(org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension.class);
    }
    
    public org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationList getOperations() {
        return this.operations;
    }
    
    public Model withOperation(org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation value) {
        getOperations().add(value);
        return (Model) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation> T createOperation(Class<T> nodeClass) {
        org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation value = createChildNode(nodeClass, "operations");
        getOperations().add(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation createOperation() {
        return createOperation(org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getDimensions()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        for (MetadataNode node : getOperations()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getDimensions()) {
            references.add(node);
        }
        
        for (MetadataNode node : getOperations()) {
            references.add(node);
        }
        
    }
}
