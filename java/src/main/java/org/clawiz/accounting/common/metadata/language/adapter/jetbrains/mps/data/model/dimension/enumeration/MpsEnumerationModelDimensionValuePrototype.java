package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsEnumerationModelDimensionValuePrototype extends AbstractMpsNode {
    
    public String description;
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845649844";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.EnumerationModelDimensionValue";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "4479303018845649844", "org.clawiz.accounting.common.language.structure.EnumerationModelDimensionValue", ConceptPropertyType.PROPERTY, "4479303018845649847", "description"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("4479303018845649844", "description", getDescription());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue structure = (org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue) node;
        
        structure.setDescription(getDescription());
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue structure = (org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue) node;
        
        setDescription(structure.getDescription());
        
    }
}
