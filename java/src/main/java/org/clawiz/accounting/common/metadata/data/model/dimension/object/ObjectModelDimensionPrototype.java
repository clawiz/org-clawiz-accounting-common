package org.clawiz.accounting.common.metadata.data.model.dimension.object;

import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ObjectModelDimensionPrototype extends org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension {
    
    @ExchangeReference
    private org.clawiz.core.common.metadata.data.type.Type referencedType;
    
    public ObjectModelDimension withName(String value) {
        setName(value);
        return (ObjectModelDimension) this;
    }
    
    public org.clawiz.core.common.metadata.data.type.Type getReferencedType() {
        return this.referencedType;
    }
    
    public void setReferencedType(org.clawiz.core.common.metadata.data.type.Type value) {
        this.referencedType = value;
    }
    
    public ObjectModelDimension withReferencedType(org.clawiz.core.common.metadata.data.type.Type value) {
        setReferencedType(value);
        return (ObjectModelDimension) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getReferencedType() != null ) { 
            getReferencedType().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getReferencedType());
        
    }
}
