package org.clawiz.accounting.common.metadata.data.type.action.accountoperation;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AccountOperationPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimensionList dimensions = new org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimensionList();
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue amount;
    
    @ExchangeReference
    private org.clawiz.accounting.common.metadata.data.model.Model accountingModel;
    
    @ExchangeReference
    private org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation operation;
    
    public AccountOperation withName(String value) {
        setName(value);
        return (AccountOperation) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimensionList getDimensions() {
        return this.dimensions;
    }
    
    public AccountOperation withDimension(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension value) {
        getDimensions().add(value);
        return (AccountOperation) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension> T createDimension(Class<T> nodeClass) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension value = createChildNode(nodeClass, "dimensions");
        getDimensions().add(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension createDimension() {
        return createDimension(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension.class);
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue getAmount() {
        return this.amount;
    }
    
    public void setAmount(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue value) {
        this.amount = value;
    }
    
    public AccountOperation withAmount(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue value) {
        setAmount(value);
        return (AccountOperation) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue> T createAmount(Class<T> nodeClass) {
        if ( getAmount() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "amount", this.getFullName());
        }
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue value = createChildNode(nodeClass, "amount");
        setAmount(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue createAmount() {
        return createAmount(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue.class);
    }
    
    public org.clawiz.accounting.common.metadata.data.model.Model getAccountingModel() {
        return this.accountingModel;
    }
    
    public void setAccountingModel(org.clawiz.accounting.common.metadata.data.model.Model value) {
        this.accountingModel = value;
    }
    
    public AccountOperation withAccountingModel(org.clawiz.accounting.common.metadata.data.model.Model value) {
        setAccountingModel(value);
        return (AccountOperation) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation getOperation() {
        return this.operation;
    }
    
    public void setOperation(org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation value) {
        this.operation = value;
    }
    
    public AccountOperation withOperation(org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation value) {
        setOperation(value);
        return (AccountOperation) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getDimensions()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        if ( getAmount() != null ) { 
            getAmount().prepare(session);
        }
        if ( getAccountingModel() != null ) { 
            getAccountingModel().prepare(session);
        }
        if ( getOperation() != null ) { 
            getOperation().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getDimensions()) {
            references.add(node);
        }
        
        references.add(getAmount());
        
        references.add(getAccountingModel());
        
        references.add(getOperation());
        
    }
}
