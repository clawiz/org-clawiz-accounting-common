package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps;

import java.lang.String;

public class MpsLanguageContextPrototype extends org.clawiz.metadata.jetbrains.mps.parser.context.AbstractMpsLanguageContext {
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public void prepare() {
        
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.MpsModel.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.object.MpsObjectModelDimension.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimension.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsModelOperation.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.MpsAccountOperationsTypeMethodAction.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.MpsAccountOperationDimension.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.MpsAccountOperation.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsAbstractAccountOperationDimensionValue.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsTypeFieldAccountOperationDimensionValue.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount.MpsAbstractAccountOperationAmountValue.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount.MpsTypeFieldAccountOperationAmountValue.class);
        addClassMap(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.value.MpsEnumerationValueAccountOperationDimensionValue.class);
        
    }
}
