package org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import org.clawiz.core.common.system.exchange.annotation.ExchangeReference;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AccountOperationDimensionPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue value;
    
    @ExchangeReference
    private org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension dimension;
    
    public AccountOperationDimension withName(String value) {
        setName(value);
        return (AccountOperationDimension) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue getValue() {
        return this.value;
    }
    
    public void setValue(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue value) {
        this.value = value;
    }
    
    public AccountOperationDimension withValue(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue value) {
        setValue(value);
        return (AccountOperationDimension) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue> T createValue(Class<T> nodeClass) {
        if ( getValue() != null ) {
            throwException("Cannot create already defined value of '?' for '?'", "value", this.getFullName());
        }
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue value = createChildNode(nodeClass, "value");
        setValue(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue createValue() {
        return createValue(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.value.AbstractAccountOperationDimensionValue.class);
    }
    
    public org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension getDimension() {
        return this.dimension;
    }
    
    public void setDimension(org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension value) {
        this.dimension = value;
    }
    
    public AccountOperationDimension withDimension(org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension value) {
        setDimension(value);
        return (AccountOperationDimension) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        if ( getValue() != null ) { 
            getValue().prepare(session);
        }
        if ( getDimension() != null ) { 
            getDimension().prepare(session);
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        references.add(getValue());
        
        references.add(getDimension());
        
    }
}
