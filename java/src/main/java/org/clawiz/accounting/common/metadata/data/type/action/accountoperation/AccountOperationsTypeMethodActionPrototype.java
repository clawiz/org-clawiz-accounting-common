package org.clawiz.accounting.common.metadata.data.type.action.accountoperation;

import org.clawiz.core.common.system.exchange.annotation.ExchangeElement;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AccountOperationsTypeMethodActionPrototype extends org.clawiz.core.common.metadata.data.type.method.action.AbstractTypeMethodAction {
    
    @ExchangeElement
    private org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationList operations = new org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationList();
    
    public AccountOperationsTypeMethodAction withName(String value) {
        setName(value);
        return (AccountOperationsTypeMethodAction) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperationList getOperations() {
        return this.operations;
    }
    
    public AccountOperationsTypeMethodAction withOperation(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation value) {
        getOperations().add(value);
        return (AccountOperationsTypeMethodAction) this;
    }
    
    public <T extends org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation> T createOperation(Class<T> nodeClass) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation value = createChildNode(nodeClass, "operations");
        getOperations().add(value);
        return (T) value;
    }
    
    public org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation createOperation() {
        return createOperation(org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation.class);
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        for (MetadataNode node : getOperations()) {
            if ( node != null ) { 
                node.prepare(session);
            }
        }
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
        for (MetadataNode node : getOperations()) {
            references.add(node);
        }
        
    }
}
