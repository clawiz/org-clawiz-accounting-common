package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAccountOperationPrototype extends AbstractMpsNode {
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.MpsAccountOperationDimension> dimensions = new ArrayList<>();
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount.MpsAbstractAccountOperationAmountValue amount;
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.MpsModel accountingModel;
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation operation;
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.MpsAccountOperationDimension> getDimensions() {
        return this.dimensions;
    }
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount.MpsAbstractAccountOperationAmountValue getAmount() {
        return this.amount;
    }
    
    public void setAmount(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.amount.MpsAbstractAccountOperationAmountValue value) {
        this.amount = value;
    }
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.MpsModel getAccountingModel() {
        return this.accountingModel;
    }
    
    public void setAccountingModel(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.MpsModel value) {
        this.accountingModel = value;
    }
    
    public org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation getOperation() {
        return this.operation;
    }
    
    public void setOperation(org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation value) {
        this.operation = value;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "2393639942922434543";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.AccountOperation";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922434543", "org.clawiz.accounting.common.language.structure.AccountOperation", ConceptPropertyType.CHILD, "2393639942922473275", "dimensions"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922434543", "org.clawiz.accounting.common.language.structure.AccountOperation", ConceptPropertyType.CHILD, "2393639942922551146", "amount"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922434543", "org.clawiz.accounting.common.language.structure.AccountOperation", ConceptPropertyType.REFERENCE, "2393639942922434557", "accountingModel"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "2393639942922434543", "org.clawiz.accounting.common.language.structure.AccountOperation", ConceptPropertyType.REFERENCE, "2393639942922434559", "operation"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        for (AbstractMpsNode value : getDimensions() ) {
            addConceptNodeChild("2393639942922434543", "dimensions", value);
        }
        addConceptNodeChild("2393639942922434543", "amount", getAmount());
        addConceptNodeRef("2393639942922434543", "accountingModel", getAccountingModel());
        addConceptNodeRef("2393639942922434543", "operation", getOperation());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation) node;
        
        structure.getDimensions().clear();
        for (org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.type.action.accountoperation.dimension.MpsAccountOperationDimension mpsNode : getDimensions() ) {
            if ( mpsNode != null ) {
                structure.getDimensions().add((org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension) mpsNode.toMetadataNode(structure, "dimensions"));
            } else {
                structure.getDimensions().add(null);
            }
        }
        
        if ( getAmount() != null ) {
            structure.setAmount((org.clawiz.accounting.common.metadata.data.type.action.accountoperation.amount.AbstractAccountOperationAmountValue) getAmount().toMetadataNode(structure, "amount"));
        } else {
            structure.setAmount(null);
        }
        
        if ( getAccountingModel() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "accountingModel", false, getAccountingModel());
        } else {
            structure.setAccountingModel(null);
        }
        
        if ( getOperation() != null ) {
            getParserContext().addDeferredParseNodeResolve(structure, "operation", false, getOperation());
        } else {
            structure.setOperation(null);
        }
        
    }
    
    public void fillForeignKeys() {
        addForeignKey(getAccountingModel());
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation structure = (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.AccountOperation) node;
        
        getDimensions().clear();
        for (org.clawiz.accounting.common.metadata.data.type.action.accountoperation.dimension.AccountOperationDimension metadataNode : structure.getDimensions() ) {
            if ( metadataNode != null ) {
                getDimensions().add(loadChildMetadataNode(metadataNode));
            } else {
                getDimensions().add(null);
            }
        }
        
        if ( structure.getAmount() != null ) {
            setAmount(loadChildMetadataNode(structure.getAmount()));
        } else {
            setAmount(null);
        }
        
        if ( structure.getAccountingModel() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "accountingModel", false, structure.getAccountingModel());
        } else {
            setAccountingModel(null);
        }
        
        if ( structure.getOperation() != null ) {
            getSolutionGeneratorContext().addDeferredSolutionNodeResolve(this, "operation", false, structure.getOperation());
        } else {
            setOperation(null);
        }
        
    }
}
