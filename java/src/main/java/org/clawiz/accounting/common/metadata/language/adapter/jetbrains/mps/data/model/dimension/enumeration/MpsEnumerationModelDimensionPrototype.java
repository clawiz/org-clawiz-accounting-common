package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsEnumerationModelDimensionPrototype extends org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension {
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue> values = new ArrayList<>();
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue> getValues() {
        return this.values;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845649843";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.EnumerationModelDimension";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.addAll(super.getConceptProperties());
        
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "4479303018845649843", "org.clawiz.accounting.common.language.structure.EnumerationModelDimension", ConceptPropertyType.CHILD, "4479303018845649885", "values"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        super.fillConceptNode();
        
        for (AbstractMpsNode value : getValues() ) {
            addConceptNodeChild("4479303018845649843", "values", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimension.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        super.fillMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimension structure = (org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimension) node;
        
        structure.getValues().clear();
        for (org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.enumeration.MpsEnumerationModelDimensionValue mpsNode : getValues() ) {
            if ( mpsNode != null ) {
                structure.getValues().add((org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue) mpsNode.toMetadataNode(structure, "values"));
            } else {
                structure.getValues().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
        super.fillForeignKeys();
    }
    
    public void loadMetadataNode(MetadataNode node) {
        super.loadMetadataNode(node);
        org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimension structure = (org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimension) node;
        
        getValues().clear();
        for (org.clawiz.accounting.common.metadata.data.model.dimension.enumeration.EnumerationModelDimensionValue metadataNode : structure.getValues() ) {
            if ( metadataNode != null ) {
                getValues().add(loadChildMetadataNode(metadataNode));
            } else {
                getValues().add(null);
            }
        }
        
    }
}
