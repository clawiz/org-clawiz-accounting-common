package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsAbstractModelDimensionPrototype extends AbstractMpsNode {
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "4479303018845565479";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.AbstractModelDimension";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension structure = (org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension) node;
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension structure = (org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension) node;
        
    }
}
