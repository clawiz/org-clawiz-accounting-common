package org.clawiz.accounting.common.metadata.data.model.operation;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class ModelOperationPrototype extends org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation {
    
    @ExchangeAttribute
    private String javaName;
    
    public ModelOperation withName(String value) {
        setName(value);
        return (ModelOperation) this;
    }
    
    public String getJavaName() {
        return this.javaName;
    }
    
    public void setJavaName(String value) {
        this.javaName = value;
    }
    
    public ModelOperation withJavaName(String value) {
        setJavaName(value);
        return (ModelOperation) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
