package org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model;

import java.math.BigDecimal;
import org.clawiz.core.common.utils.StringUtils;
import org.clawiz.metadata.jetbrains.mps.data.AbstractMpsNode;
import java.util.ArrayList;
import org.clawiz.core.common.metadata.node.MetadataNode;

public class MpsModelPrototype extends AbstractMpsNode {
    
    public String description;
    
    public String javaName;
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension> dimensions = new ArrayList<>();
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation> operations = new ArrayList<>();
    
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.description = null;
        }
        this.description = value;
    }
    
    public String getJavaName() {
        return this.javaName;
    }
    
    public void setJavaName(String value) {
        if ( StringUtils.isEmpty(value) ) {
            this.javaName = null;
        }
        this.javaName = value;
    }
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension> getDimensions() {
        return this.dimensions;
    }
    
    public ArrayList<org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation> getOperations() {
        return this.operations;
    }
    
    public String getLanguageId() {
        return "f34c144b-72e4-416d-98eb-83c7769a8947";
    }
    
    public String getLanguageName() {
        return "org.clawiz.accounting.common.language";
    }
    
    public String getLanguageConceptId() {
        return "8748341616665097964";
    }
    
    public String getLanguageConceptName() {
        return "org.clawiz.accounting.common.language.structure.Model";
    }
    
    public ArrayList<ConceptProperty> getConceptProperties() {
        ArrayList<ConceptProperty> result = new ArrayList<>();
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "8748341616665097964", "org.clawiz.accounting.common.language.structure.Model", ConceptPropertyType.PROPERTY, "4479303018845643101", "description"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "8748341616665097964", "org.clawiz.accounting.common.language.structure.Model", ConceptPropertyType.PROPERTY, "2393639942922416152", "javaName"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "8748341616665097964", "org.clawiz.accounting.common.language.structure.Model", ConceptPropertyType.CHILD, "4479303018845565484", "dimensions"));
        result.add(new ConceptProperty("f34c144b-72e4-416d-98eb-83c7769a8947", "org.clawiz.accounting.common.language", "8748341616665097964", "org.clawiz.accounting.common.language.structure.Model", ConceptPropertyType.CHILD, "4479303018845845896", "operations"));
        
        return result;
    }
    
    public void fillConceptNode() {
        
        addConceptNodeProperty("1169194658468", "name", getName());
        addConceptNodeProperty("8748341616665097964", "description", getDescription());
        addConceptNodeProperty("8748341616665097964", "javaName", getJavaName());
        for (AbstractMpsNode value : getDimensions() ) {
            addConceptNodeChild("8748341616665097964", "dimensions", value);
        }
        for (AbstractMpsNode value : getOperations() ) {
            addConceptNodeChild("8748341616665097964", "operations", value);
        }
        
    }
    
    public <T extends MetadataNode> Class<T> getMetadataNodeClass() {
        return (Class<T>) org.clawiz.accounting.common.metadata.data.model.Model.class;
    }
    
    protected void fillMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.Model structure = (org.clawiz.accounting.common.metadata.data.model.Model) node;
        
        structure.setDescription(getDescription());
        
        structure.setJavaName(getJavaName());
        
        structure.getDimensions().clear();
        for (org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.dimension.MpsAbstractModelDimension mpsNode : getDimensions() ) {
            if ( mpsNode != null ) {
                structure.getDimensions().add((org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension) mpsNode.toMetadataNode(structure, "dimensions"));
            } else {
                structure.getDimensions().add(null);
            }
        }
        
        structure.getOperations().clear();
        for (org.clawiz.accounting.common.metadata.language.adapter.jetbrains.mps.data.model.operation.MpsAbstractModelOperation mpsNode : getOperations() ) {
            if ( mpsNode != null ) {
                structure.getOperations().add((org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation) mpsNode.toMetadataNode(structure, "operations"));
            } else {
                structure.getOperations().add(null);
            }
        }
        
    }
    
    public void fillForeignKeys() {
    }
    
    public void loadMetadataNode(MetadataNode node) {
        org.clawiz.accounting.common.metadata.data.model.Model structure = (org.clawiz.accounting.common.metadata.data.model.Model) node;
        
        setDescription(structure.getDescription());
        
        setJavaName(structure.getJavaName());
        
        getDimensions().clear();
        for (org.clawiz.accounting.common.metadata.data.model.dimension.AbstractModelDimension metadataNode : structure.getDimensions() ) {
            if ( metadataNode != null ) {
                getDimensions().add(loadChildMetadataNode(metadataNode));
            } else {
                getDimensions().add(null);
            }
        }
        
        getOperations().clear();
        for (org.clawiz.accounting.common.metadata.data.model.operation.AbstractModelOperation metadataNode : structure.getOperations() ) {
            if ( metadataNode != null ) {
                getOperations().add(loadChildMetadataNode(metadataNode));
            } else {
                getOperations().add(null);
            }
        }
        
    }
}
