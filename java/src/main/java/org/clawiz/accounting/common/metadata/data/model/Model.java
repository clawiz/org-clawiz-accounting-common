package org.clawiz.accounting.common.metadata.data.model;


import org.clawiz.core.common.metadata.install.AbstractMetadataNodeInstaller;
import org.clawiz.core.common.metadata.install.EmptyMetadataNodeInstaller;

public class Model extends ModelPrototype {

    @Override
    public <T extends AbstractMetadataNodeInstaller> Class<T> getInstallerClass() {
        return (Class<T>) EmptyMetadataNodeInstaller.class;
    }
}
