package org.clawiz.accounting.common.metadata.data.model.operation;

import org.clawiz.core.common.system.exchange.annotation.ExchangeAttribute;
import java.lang.SuppressWarnings;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.core.common.metadata.node.MetadataNodeReferencesList;

public class AbstractModelOperationPrototype extends org.clawiz.core.common.metadata.node.MetadataNode {
    
    @ExchangeAttribute
    private org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationSign sign;
    
    public AbstractModelOperation withName(String value) {
        setName(value);
        return (AbstractModelOperation) this;
    }
    
    public org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationSign getSign() {
        return this.sign;
    }
    
    public void setSign(org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationSign value) {
        this.sign = value;
    }
    
    public AbstractModelOperation withSign(org.clawiz.accounting.common.metadata.data.model.operation.ModelOperationSign value) {
        setSign(value);
        return (AbstractModelOperation) this;
    }
    
    @SuppressWarnings("Duplicates")
    public void prepare(Session session) {
        super.prepare(session);
        if ( isInPrepare() ) { return; }
        setInPrepare(true);
        
        
        setInPrepare(false);
    }
    
    public void fillReferences(MetadataNodeReferencesList references) {
        super.fillReferences(references);
        
    }
}
