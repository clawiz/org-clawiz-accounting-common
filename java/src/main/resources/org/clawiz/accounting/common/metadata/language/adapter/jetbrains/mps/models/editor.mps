<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3971fd48-89ac-4c80-a4e9-826031047129(org.clawiz.accounting.common.language.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="jmf2" ref="r:dd6e0111-1ac0-43ca-aff4-28c272266a27(org.clawiz.accounting.common.language.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="928328222691832421" name="separatorTextQuery" index="2gpyvW" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="709996738298806197" name="jetbrains.mps.lang.editor.structure.QueryFunction_SeparatorText" flags="in" index="2o9xnK" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1215007883204" name="jetbrains.mps.lang.editor.structure.PaddingLeftStyleClassItem" flags="ln" index="3$7fVu" />
      <concept id="1215007897487" name="jetbrains.mps.lang.editor.structure.PaddingRightStyleClassItem" flags="ln" index="3$7jql" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="3SDF_NZBZ8k">
    <property role="3GE5qa" value="model" />
    <ref role="1XX52x" to="jmf2:7_Cm_A35JrG" resolve="Model" />
    <node concept="3EZMnI" id="3SDF_NZBZ8m" role="2wV5jI">
      <node concept="3F0ifn" id="3SDF_NZBZ8t" role="3EZMnx">
        <property role="3F0ifm" value="Account model" />
      </node>
      <node concept="3F0A7n" id="3SDF_NZBZ8z" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="3SDF_NZD0HN" role="3EZMnx">
        <node concept="pVoyu" id="3SDF_NZD0HO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZCZnP" role="3EZMnx">
        <property role="3F0ifm" value="       javaName    " />
        <node concept="pVoyu" id="3SDF_NZCZnQ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="24RUHym4V1t" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym4V0o" resolve="javaName" />
      </node>
      <node concept="3F0ifn" id="24RUHym4V0r" role="3EZMnx">
        <property role="3F0ifm" value="       description " />
        <node concept="pVoyu" id="24RUHym4V0s" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="3SDF_NZCi5v" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZCi5t" resolve="description" />
      </node>
      <node concept="3F0ifn" id="3SDF_NZCi4Z" role="3EZMnx">
        <node concept="pVoyu" id="3SDF_NZCi50" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZC1mz" role="3EZMnx">
        <property role="3F0ifm" value="dimensions " />
        <node concept="pVoyu" id="3SDF_NZC1m$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZCYuO" role="3EZMnx">
        <node concept="pVoyu" id="3SDF_NZCYuP" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="3SDF_NZC1n1" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZBZ8G" resolve="dimensions" />
        <node concept="2EHx9g" id="3SDF_NZC1o7" role="2czzBx" />
        <node concept="VPM3Z" id="3SDF_NZC1n5" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="lj46D" id="3SDF_NZCYvg" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="3SDF_NZCYvo" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZD3BA" role="3EZMnx">
        <node concept="pVoyu" id="3SDF_NZD3BB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZD3Be" role="3EZMnx">
        <property role="3F0ifm" value="operations" />
        <node concept="pVoyu" id="3SDF_NZD3Bf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="3SDF_NZD3AS" role="3EZMnx">
        <node concept="pVoyu" id="3SDF_NZD3AT" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="3SDF_NZD3De" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZD3A8" resolve="operations" />
        <node concept="2EHx9g" id="3SDF_NZD5QD" role="2czzBx" />
        <node concept="VPM3Z" id="3SDF_NZD3Di" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="lj46D" id="3SDF_NZD3DL" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="3SDF_NZD3DT" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="3SDF_NZBZ8p" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3SDF_NZCjJ1">
    <property role="3GE5qa" value="model.dimension.enumeration" />
    <ref role="1XX52x" to="jmf2:3SDF_NZCjIO" resolve="EnumerationModelDimensionValue" />
    <node concept="3EZMnI" id="3SDF_NZCjJ3" role="2wV5jI">
      <node concept="3F0A7n" id="3SDF_NZCjJa" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="3SDF_NZCjJg" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F0A7n" id="3SDF_NZCjJo" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZCjIR" resolve="description" />
      </node>
      <node concept="2iRfu4" id="3SDF_NZCjJ6" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3SDF_NZCjJv">
    <property role="3GE5qa" value="model.dimension.enumeration" />
    <ref role="1XX52x" to="jmf2:3SDF_NZCjIN" resolve="EnumerationModelDimension" />
    <node concept="3EZMnI" id="3SDF_NZCjJx" role="2wV5jI">
      <node concept="3F0A7n" id="3SDF_NZClT0" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="3SDF_NZCjJC" role="3EZMnx">
        <property role="3F0ifm" value="enum" />
      </node>
      <node concept="3F2HdR" id="3SDF_NZCjJI" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZCjJt" resolve="values" />
        <node concept="2EHx9g" id="3SDF_NZCjJR" role="2czzBx" />
        <node concept="VPM3Z" id="3SDF_NZCjJM" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
      </node>
      <node concept="2iRfu4" id="3SDF_NZCjJ$" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3SDF_NZCko5">
    <property role="3GE5qa" value="model.dimension.object" />
    <ref role="1XX52x" to="jmf2:3SDF_NZCjIK" resolve="ObjectModelDimension" />
    <node concept="3EZMnI" id="3SDF_NZCko7" role="2wV5jI">
      <node concept="3F0A7n" id="3SDF_NZCkoe" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="3SDF_NZCkok" role="3EZMnx">
        <property role="3F0ifm" value="-&gt;" />
      </node>
      <node concept="1iCGBv" id="3SDF_NZCkos" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZCjIL" resolve="referencedType" />
        <node concept="1sVBvm" id="3SDF_NZCkou" role="1sWHZn">
          <node concept="3F0A7n" id="3SDF_NZCkoB" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="3SDF_NZCkoa" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="3SDF_NZD3Ac">
    <property role="3GE5qa" value="model.operation" />
    <ref role="1XX52x" to="jmf2:3SDF_NZD3Ab" resolve="ModelOperation" />
    <node concept="3EZMnI" id="3SDF_NZD4VD" role="2wV5jI">
      <node concept="3F0A7n" id="3SDF_NZD4VK" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="3SDF_NZD4VQ" role="3EZMnx">
        <property role="3F0ifm" value="(" />
      </node>
      <node concept="3F0A7n" id="3SDF_NZD4VY" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:3SDF_NZD3A6" resolve="sign" />
      </node>
      <node concept="3F0ifn" id="24RUHym4YfE" role="3EZMnx">
        <property role="3F0ifm" value=")" />
      </node>
      <node concept="3F0A7n" id="24RUHym4WCf" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym4WC8" resolve="javaName" />
      </node>
      <node concept="2iRfu4" id="3SDF_NZD4VG" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="24RUHym4v_a">
    <property role="3GE5qa" value="type.action.accountoperation" />
    <ref role="1XX52x" to="jmf2:3SDF_NZD6Pz" resolve="AccountOperationsTypeMethodAction" />
    <node concept="3EZMnI" id="24RUHym51N_" role="2wV5jI">
      <node concept="3F0ifn" id="24RUHym51NG" role="3EZMnx">
        <property role="3F0ifm" value="accounting operations" />
      </node>
      <node concept="3F2HdR" id="24RUHym51NM" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym4ZvO" resolve="operations" />
        <node concept="2EHx9g" id="24RUHym51NV" role="2czzBx" />
        <node concept="VPM3Z" id="24RUHym51NQ" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="2o9xnK" id="24RUHym58YG" role="2gpyvW">
          <node concept="3clFbS" id="24RUHym58YH" role="2VODD2">
            <node concept="3cpWs6" id="24RUHym5975" role="3cqZAp">
              <node concept="Xl_RD" id="24RUHym59fy" role="3cqZAk">
                <property role="Xl_RC" value="" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="24RUHym51NC" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="24RUHym4Zwa">
    <property role="3GE5qa" value="type.action.accountoperation" />
    <ref role="1XX52x" to="jmf2:24RUHym4ZvJ" resolve="AccountOperation" />
    <node concept="3EZMnI" id="24RUHym4ZwJ" role="2wV5jI">
      <node concept="3F0ifn" id="24RUHym4ZwQ" role="3EZMnx">
        <property role="3F0ifm" value="model      " />
      </node>
      <node concept="1iCGBv" id="24RUHym4ZwW" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym4ZvX" resolve="accountingModel" />
        <node concept="1sVBvm" id="24RUHym4ZwY" role="1sWHZn">
          <node concept="3F0A7n" id="24RUHym4Zx6" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="24RUHym4Zxf" role="3EZMnx">
        <property role="3F0ifm" value="operation  " />
        <node concept="pVoyu" id="24RUHym4Zxm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="24RUHym4ZyU" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym4ZvZ" resolve="operation" />
        <node concept="1sVBvm" id="24RUHym4ZyW" role="1sWHZn">
          <node concept="3F0A7n" id="24RUHym4Zzj" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="24RUHym5rY0" role="3EZMnx">
        <property role="3F0ifm" value="amount     " />
        <node concept="pVoyu" id="24RUHym5rYi" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="24RUHym5rYB" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym5rXE" resolve="amount" />
      </node>
      <node concept="3F0ifn" id="24RUHym58XA" role="3EZMnx">
        <property role="3F0ifm" value="dimensions " />
        <node concept="pVoyu" id="24RUHym58XB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="24RUHym58Y4" role="3EZMnx">
        <node concept="3F2HdR" id="24RUHym58Yo" role="3EZMnx">
          <ref role="1NtTu8" to="jmf2:24RUHym58WV" resolve="dimensions" />
          <node concept="2EHx9g" id="24RUHym5i6S" role="2czzBx" />
        </node>
        <node concept="2iRkQZ" id="24RUHym58Y7" role="2iSdaV" />
      </node>
      <node concept="l2Vlx" id="24RUHym4ZwM" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="24RUHym59o6">
    <property role="3GE5qa" value="type.action.accountoperation.dimension" />
    <ref role="1XX52x" to="jmf2:24RUHym4V0b" resolve="AccountOperationDimension" />
    <node concept="3EZMnI" id="24RUHym59o8" role="2wV5jI">
      <node concept="1iCGBv" id="24RUHym59of" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym58WT" resolve="dimension" />
        <node concept="1sVBvm" id="24RUHym59oh" role="1sWHZn">
          <node concept="3F0A7n" id="24RUHym59oo" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="24RUHym59ow" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F1sOY" id="24RUHym59oJ" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym59oG" resolve="value" />
      </node>
      <node concept="2iRfu4" id="24RUHym59ob" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="24RUHym5ck5">
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <ref role="1XX52x" to="jmf2:24RUHym5cjU" resolve="TypeFieldAccountOperationDimensionValue" />
    <node concept="3EZMnI" id="24RUHym5ckf" role="2wV5jI">
      <node concept="3F0ifn" id="24RUHym5fSX" role="3EZMnx">
        <property role="3F0ifm" value="field" />
        <node concept="3$7jql" id="24RUHym5n1u" role="3F10Kt">
          <property role="3$6WeP" value="0" />
        </node>
      </node>
      <node concept="1iCGBv" id="24RUHym5ckm" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym5cjV" resolve="field" />
        <node concept="1sVBvm" id="24RUHym5cko" role="1sWHZn">
          <node concept="3F0A7n" id="24RUHym5ckv" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
        <node concept="3$7fVu" id="24RUHym5n1s" role="3F10Kt">
          <property role="3$6WeP" value="0" />
        </node>
      </node>
      <node concept="2iRfu4" id="24RUHym5cki" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="24RUHym5rZ5">
    <property role="3GE5qa" value="type.action.accountoperation.amount" />
    <ref role="1XX52x" to="jmf2:24RUHym5rYV" resolve="TypeFieldAccountOperationAmountValue" />
    <node concept="3EZMnI" id="24RUHym5rZ7" role="2wV5jI">
      <node concept="3F0ifn" id="24RUHym5rZe" role="3EZMnx">
        <property role="3F0ifm" value="field" />
      </node>
      <node concept="1iCGBv" id="24RUHym5rZk" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym5rYW" resolve="field" />
        <node concept="1sVBvm" id="24RUHym5rZm" role="1sWHZn">
          <node concept="3F0A7n" id="24RUHym5rZu" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="24RUHym5rZa" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="24RUHym5_17">
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <ref role="1XX52x" to="jmf2:24RUHym5_0W" resolve="EnumerationValueAccountOperationDimensionValue" />
    <node concept="3EZMnI" id="24RUHym5_19" role="2wV5jI">
      <node concept="3F0ifn" id="24RUHym5_1g" role="3EZMnx">
        <property role="3F0ifm" value="value" />
      </node>
      <node concept="1iCGBv" id="24RUHym5_1m" role="3EZMnx">
        <ref role="1NtTu8" to="jmf2:24RUHym5_0X" resolve="value" />
        <node concept="1sVBvm" id="24RUHym5_1o" role="1sWHZn">
          <node concept="3F0A7n" id="24RUHym5_1w" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="24RUHym5_1c" role="2iSdaV" />
    </node>
  </node>
</model>

