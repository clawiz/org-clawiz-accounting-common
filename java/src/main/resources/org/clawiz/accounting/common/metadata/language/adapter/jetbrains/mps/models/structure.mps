<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:dd6e0111-1ac0-43ca-aff4-28c272266a27(org.clawiz.accounting.common.language.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <use id="97fd598e-b769-49ad-bf12-ef327309f6db" name="org.clawiz.core.common.language" version="0" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1082978164219" name="jetbrains.mps.lang.structure.structure.EnumerationDataTypeDeclaration" flags="ng" index="AxPO7">
        <property id="1197591154882" name="memberIdentifierPolicy" index="3lZH7k" />
        <reference id="1083171729157" name="memberDataType" index="M4eZT" />
        <reference id="1083241965437" name="defaultMember" index="Qgau1" />
        <child id="1083172003582" name="member" index="M5hS2" />
      </concept>
      <concept id="1083171877298" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ig" index="M4N5e">
        <property id="1083923523172" name="externalValue" index="1uS6qo" />
        <property id="1083923523171" name="internalValue" index="1uS6qv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="7_Cm_A35JrG">
    <property role="EcuMT" value="8748341616665097964" />
    <property role="TrG5h" value="Model" />
    <property role="3GE5qa" value="model" />
    <property role="34LRSv" value="Account model" />
    <property role="19KtqR" value="true" />
    <node concept="1TJgyj" id="3SDF_NZBZ8G" role="1TKVEi">
      <property role="IQ2ns" value="4479303018845565484" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dimensions" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="3SDF_NZBZ8B" resolve="AbstractModelDimension" />
    </node>
    <node concept="1TJgyj" id="3SDF_NZD3A8" role="1TKVEi">
      <property role="IQ2ns" value="4479303018845845896" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="operations" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="3SDF_NZD3_V" resolve="AbstractModelOperation" />
    </node>
    <node concept="PrWs8" id="7_Cm_A35JrH" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="7_Cm_A35JrM" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyi" id="3SDF_NZCi5t" role="1TKVEl">
      <property role="IQ2nx" value="4479303018845643101" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="24RUHym4V0o" role="1TKVEl">
      <property role="IQ2nx" value="2393639942922416152" />
      <property role="TrG5h" value="javaName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZBZ8B">
    <property role="EcuMT" value="4479303018845565479" />
    <property role="3GE5qa" value="model.dimension" />
    <property role="TrG5h" value="AbstractModelDimension" />
    <property role="R5$K7" value="true" />
    <node concept="PrWs8" id="3SDF_NZBZ8C" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="24RUHym5I3p" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZCjIK">
    <property role="EcuMT" value="4479303018845649840" />
    <property role="3GE5qa" value="model.dimension.object" />
    <property role="TrG5h" value="ObjectModelDimension" />
    <property role="34LRSv" value="-&gt;" />
    <ref role="1TJDcQ" node="3SDF_NZBZ8B" resolve="AbstractModelDimension" />
    <node concept="1TJgyj" id="3SDF_NZCjIL" role="1TKVEi">
      <property role="IQ2ns" value="4479303018845649841" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="referencedType" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmg18" resolve="Type" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZCjIN">
    <property role="EcuMT" value="4479303018845649843" />
    <property role="3GE5qa" value="model.dimension.enumeration" />
    <property role="TrG5h" value="EnumerationModelDimension" />
    <property role="34LRSv" value="enum" />
    <ref role="1TJDcQ" node="3SDF_NZBZ8B" resolve="AbstractModelDimension" />
    <node concept="1TJgyj" id="3SDF_NZCjJt" role="1TKVEi">
      <property role="IQ2ns" value="4479303018845649885" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="values" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="3SDF_NZCjIO" resolve="EnumerationModelDimensionValue" />
    </node>
    <node concept="PrWs8" id="24RUHym5VUG" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZCjIO">
    <property role="EcuMT" value="4479303018845649844" />
    <property role="3GE5qa" value="model.dimension.enumeration" />
    <property role="TrG5h" value="EnumerationModelDimensionValue" />
    <node concept="PrWs8" id="3SDF_NZCjIP" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="3SDF_NZCjIR" role="1TKVEl">
      <property role="IQ2nx" value="4479303018845649847" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZD3_V">
    <property role="EcuMT" value="4479303018845845883" />
    <property role="3GE5qa" value="model.operation" />
    <property role="TrG5h" value="AbstractModelOperation" />
    <property role="R5$K7" value="true" />
    <node concept="PrWs8" id="3SDF_NZD3_W" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="3SDF_NZD3A6" role="1TKVEl">
      <property role="IQ2nx" value="4479303018845845894" />
      <property role="TrG5h" value="sign" />
      <ref role="AX2Wp" node="3SDF_NZD3A1" resolve="ModelOperationSign" />
    </node>
  </node>
  <node concept="AxPO7" id="3SDF_NZD3A1">
    <property role="3GE5qa" value="model.operation" />
    <property role="TrG5h" value="ModelOperationSign" />
    <property role="3lZH7k" value="derive_from_internal_value" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <ref role="Qgau1" node="3SDF_NZD3A2" />
    <node concept="M4N5e" id="3SDF_NZD3A2" role="M5hS2">
      <property role="1uS6qo" value="+" />
      <property role="1uS6qv" value="PLUS" />
    </node>
    <node concept="M4N5e" id="3SDF_NZD3A3" role="M5hS2">
      <property role="1uS6qo" value="-" />
      <property role="1uS6qv" value="MINUS" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZD3Ab">
    <property role="EcuMT" value="4479303018845845899" />
    <property role="3GE5qa" value="model.operation" />
    <property role="TrG5h" value="ModelOperation" />
    <ref role="1TJDcQ" node="3SDF_NZD3_V" resolve="AbstractModelOperation" />
    <node concept="1TJgyi" id="24RUHym4WC8" role="1TKVEl">
      <property role="IQ2nx" value="2393639942922422792" />
      <property role="TrG5h" value="javaName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="3SDF_NZD6Pz">
    <property role="EcuMT" value="4479303018845859171" />
    <property role="TrG5h" value="AccountOperationsTypeMethodAction" />
    <property role="3GE5qa" value="type.action.accountoperation" />
    <property role="34LRSv" value="accounting operations" />
    <ref role="1TJDcQ" to="lehn:yAB$tbwYhU" resolve="AbstractTypeMethodAction" />
    <node concept="1TJgyj" id="24RUHym4ZvO" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922434548" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="operations" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="24RUHym4ZvJ" resolve="AccountOperation" />
    </node>
  </node>
  <node concept="1TIwiD" id="24RUHym4V0b">
    <property role="EcuMT" value="2393639942922416139" />
    <property role="3GE5qa" value="type.action.accountoperation.dimension" />
    <property role="TrG5h" value="AccountOperationDimension" />
    <node concept="1TJgyj" id="24RUHym59oG" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922475052" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="24RUHym59o5" resolve="AbstractAccountOperationDimensionValue" />
    </node>
    <node concept="1TJgyj" id="24RUHym58WT" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922473273" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="dimension" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="3SDF_NZBZ8B" resolve="AbstractModelDimension" />
    </node>
    <node concept="PrWs8" id="24RUHym5GyO" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="24RUHym4ZvJ">
    <property role="EcuMT" value="2393639942922434543" />
    <property role="3GE5qa" value="type.action.accountoperation" />
    <property role="TrG5h" value="AccountOperation" />
    <property role="34LRSv" value="operation" />
    <node concept="1TJgyj" id="24RUHym58WV" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922473275" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dimensions" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="24RUHym4V0b" resolve="AccountOperationDimension" />
    </node>
    <node concept="1TJgyj" id="24RUHym5rXE" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922551146" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="amount" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="24RUHym5rXB" resolve="AbstractAccountOperationAmountValue" />
    </node>
    <node concept="1TJgyj" id="24RUHym4ZvX" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922434557" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="accountingModel" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="7_Cm_A35JrG" resolve="Model" />
    </node>
    <node concept="1TJgyj" id="24RUHym4ZvZ" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922434559" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="operation" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="3SDF_NZD3_V" resolve="AbstractModelOperation" />
    </node>
    <node concept="PrWs8" id="24RUHym4ZvK" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="24RUHym54MC" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="24RUHym59o5">
    <property role="EcuMT" value="2393639942922475013" />
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <property role="TrG5h" value="AbstractAccountOperationDimensionValue" />
    <property role="R5$K7" value="true" />
  </node>
  <node concept="1TIwiD" id="24RUHym5cjU">
    <property role="EcuMT" value="2393639942922487034" />
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <property role="TrG5h" value="TypeFieldAccountOperationDimensionValue" />
    <property role="34LRSv" value="field" />
    <ref role="1TJDcQ" node="24RUHym59o5" resolve="AbstractAccountOperationDimensionValue" />
    <node concept="1TJgyj" id="24RUHym5cjV" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922487035" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="field" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmPQb" resolve="TypeField" />
    </node>
  </node>
  <node concept="1TIwiD" id="24RUHym5rXB">
    <property role="EcuMT" value="2393639942922551143" />
    <property role="3GE5qa" value="type.action.accountoperation.amount" />
    <property role="TrG5h" value="AbstractAccountOperationAmountValue" />
    <property role="R5$K7" value="true" />
  </node>
  <node concept="1TIwiD" id="24RUHym5rYV">
    <property role="EcuMT" value="2393639942922551227" />
    <property role="3GE5qa" value="type.action.accountoperation.amount" />
    <property role="TrG5h" value="TypeFieldAccountOperationAmountValue" />
    <property role="34LRSv" value="field" />
    <ref role="1TJDcQ" node="24RUHym5rXB" resolve="AbstractAccountOperationAmountValue" />
    <node concept="1TJgyj" id="24RUHym5rYW" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922551228" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="field" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="lehn:6porqNrmPQb" resolve="TypeField" />
    </node>
  </node>
  <node concept="1TIwiD" id="24RUHym5_0W">
    <property role="EcuMT" value="2393639942922588220" />
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <property role="TrG5h" value="EnumerationValueAccountOperationDimensionValue" />
    <property role="34LRSv" value="value" />
    <ref role="1TJDcQ" node="24RUHym59o5" resolve="AbstractAccountOperationDimensionValue" />
    <node concept="1TJgyj" id="24RUHym5_0X" role="1TKVEi">
      <property role="IQ2ns" value="2393639942922588221" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="3SDF_NZCjIO" resolve="EnumerationModelDimensionValue" />
    </node>
    <node concept="PrWs8" id="24RUHym5FCE" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
</model>

