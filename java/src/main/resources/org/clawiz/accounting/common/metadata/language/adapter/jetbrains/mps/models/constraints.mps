<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:dbe20a51-f4d3-488d-9048-774cfd9c163a(org.clawiz.accounting.common.language.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="jmf2" ref="r:dd6e0111-1ac0-43ca-aff4-28c272266a27(org.clawiz.accounting.common.language.structure)" implicit="true" />
    <import index="lehn" ref="r:f00fd6de-b38e-411a-9521-a0b7f412e1e8(org.clawiz.core.common.language.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="24RUHym4Zzx">
    <property role="3GE5qa" value="type.action.accountoperation" />
    <ref role="1M2myG" to="jmf2:24RUHym4ZvJ" resolve="AccountOperation" />
    <node concept="1N5Pfh" id="24RUHym4Zzy" role="1Mr941">
      <ref role="1N5Vy1" to="jmf2:24RUHym4ZvZ" resolve="operation" />
      <node concept="1dDu$B" id="24RUHym4Zz$" role="1N6uqs">
        <ref role="1dDu$A" to="jmf2:3SDF_NZD3_V" resolve="AbstractModelOperation" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="24RUHym5cky">
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <ref role="1M2myG" to="jmf2:24RUHym5cjU" resolve="TypeFieldAccountOperationDimensionValue" />
    <node concept="1N5Pfh" id="24RUHym5ckz" role="1Mr941">
      <ref role="1N5Vy1" to="jmf2:24RUHym5cjV" resolve="field" />
      <node concept="1dDu$B" id="24RUHym5ck_" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmPQb" resolve="TypeField" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="24RUHym5f73">
    <property role="3GE5qa" value="type.action.accountoperation.dimension" />
    <ref role="1M2myG" to="jmf2:24RUHym4V0b" resolve="AccountOperationDimension" />
    <node concept="1N5Pfh" id="24RUHym5f74" role="1Mr941">
      <ref role="1N5Vy1" to="jmf2:24RUHym58WT" resolve="dimension" />
      <node concept="1dDu$B" id="24RUHym5f76" role="1N6uqs">
        <ref role="1dDu$A" to="jmf2:3SDF_NZBZ8B" resolve="AbstractModelDimension" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="24RUHym5rZx">
    <property role="3GE5qa" value="type.action.accountoperation.amount" />
    <ref role="1M2myG" to="jmf2:24RUHym5rYV" resolve="TypeFieldAccountOperationAmountValue" />
    <node concept="1N5Pfh" id="24RUHym5rZy" role="1Mr941">
      <ref role="1N5Vy1" to="jmf2:24RUHym5rYW" resolve="field" />
      <node concept="1dDu$B" id="24RUHym5rZ$" role="1N6uqs">
        <ref role="1dDu$A" to="lehn:6porqNrmPQb" resolve="TypeField" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="24RUHym5F_x">
    <property role="3GE5qa" value="type.action.accountoperation.dimension.value" />
    <ref role="1M2myG" to="jmf2:24RUHym5_0W" resolve="EnumerationValueAccountOperationDimensionValue" />
    <node concept="1N5Pfh" id="24RUHym5F_y" role="1Mr941">
      <ref role="1N5Vy1" to="jmf2:24RUHym5_0X" resolve="value" />
      <node concept="1dDu$B" id="24RUHym5FCt" role="1N6uqs">
        <ref role="1dDu$A" to="jmf2:3SDF_NZCjIO" resolve="EnumerationModelDimensionValue" />
      </node>
    </node>
  </node>
</model>

